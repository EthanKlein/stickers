# Sticker pack

## Adding stickers
Run `sticker-pack directory-here --title title-here --add-to-index web/packs`, enter your homeserver URL and [access token](https://youtu.be/Yz3H6KJTEI0?t=249) then it should start uploading your stickers and you can create a pullrequest.
